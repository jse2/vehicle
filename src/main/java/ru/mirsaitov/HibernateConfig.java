package ru.mirsaitov;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.mirsaitov.entity.Bus;
import ru.mirsaitov.entity.ElectricCar;
import ru.mirsaitov.entity.SportsCar;
import ru.mirsaitov.entity.Truck;

public class HibernateConfig{

    private static SessionFactory sessionFactory;

    private HibernateConfig() {
    };

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) {
            return sessionFactory;
        }

        Configuration cfx = new Configuration();
        cfx.addAnnotatedClass(SportsCar.class);
        cfx.addAnnotatedClass(Bus.class);
        cfx.addAnnotatedClass(ElectricCar.class);
        cfx.addAnnotatedClass(Truck.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfx.getProperties())
                .build();

        sessionFactory = cfx.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

}
