package ru.mirsaitov.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;

import static javax.persistence.InheritanceType.JOINED;

@Getter
@Setter
@Entity
@Table(name = "vehicle")
@Inheritance(strategy = JOINED)
@Accessors(chain = true)
public abstract class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(length = 255)
    private String brand;

    @Column(length = 255)
    private String model;

    LocalDate year;

}
