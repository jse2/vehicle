package ru.mirsaitov.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "bus")
@Accessors(chain = true)
public class Bus extends Vehicle {

    @Column(name = "max_passengers")
    private int maxPassengers;

}
