package ru.mirsaitov.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "electric_car")
@Accessors(chain = true)
public class ElectricCar extends Vehicle {

    @Column(name = "max_distance")
    private int maxDistance;

}