package ru.mirsaitov.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "sports_car")
@Accessors(chain = true)
public class SportsCar extends Vehicle {

    @Column(name = "horsepower")
    private int horsePower;

    private int acceleration;

}
