package ru.mirsaitov.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "truck")
@Accessors(chain = true)
public class Truck extends Vehicle {

    private int carrying;

}
