package ru.mirsaitov;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.mirsaitov.entity.Bus;
import ru.mirsaitov.entity.ElectricCar;
import ru.mirsaitov.entity.SportsCar;
import ru.mirsaitov.entity.Truck;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        SportsCar sportsCar = (SportsCar)new SportsCar().setHorsePower(200)
                .setAcceleration(4)
                .setBrand("mazda")
                .setYear(LocalDate.now())
                .setModel("z5");

        Bus bus = (Bus)new Bus().setMaxPassengers(200)
                .setBrand("zil")
                .setYear(LocalDate.now())
                .setModel("pazik");

        ElectricCar electricCar = (ElectricCar)new ElectricCar().setMaxDistance(1000)
                .setBrand("tesla")
                .setYear(LocalDate.now())
                .setModel("test");

        Truck truck = (Truck)new Truck().setCarrying(200)
                .setBrand("kamaz")
                .setYear(LocalDate.now())
                .setModel("new");

        save(sportsCar);
        save(bus);
        save(electricCar);
        save(truck);

    }

    private static <T> Long save(T entity) {
        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Long id = (Long)session.save(entity);
            transaction.commit();
            return id;
        }
    }

}
